---
title: Mail server maintenance
date: 2023-10-11 09:50:00
resolved: true
resolvedWhen: 2023-10-11 19:30:00
# down, disrupted, notice
severity: disrupted
affected:
  - Wiki
section: issue
---

*Announcement* -
E-Mail server is down for maintenance, registration mails from the wiki will
currently not arrive.
