---
title: IPv6 connectivity added
date: 2021-07-06 11:39:00
resolved: true
resolvedWhen: 2021-07-06 11:39:00
# down, disrupted, notice
severity: notice
affected:
  - Wiki
  - Build manager
  - Website
  - Binary repository
  - Matrix
section: issue
informational: true
---

IPv6 connectivity has been enabled on all the postmarketOS services now. This might
cause outages if your traffic is suddenly routed over other protocols or one of the
services isn't listening on IPv6
