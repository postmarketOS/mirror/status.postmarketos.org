---
title: DNS migration
date: 2023-05-02 23:00:00
resolved: true
resolvedWhen: 2023-05-03 15:53:00
# down, disrupted, notice
severity: disrupted
affected:
  - Website
section: issue
---

*Resolved* - The DNS record is fixed, it will take up to 12 hours for the change to update on all DNS providers.

The DNS zone for postmarketos.org is migrate to a new host. The CNAME record for cast.postmarketos.org is pointing to the wrong server.
