---
title: Mirror and build system migration
date: 2023-06-15 18:10:00
resolved: true
resolvedWhen: 2023-06-16 18:10:00
# down, disrupted, notice
severity: notice
affected:
  - Build manager
  - Binary repository
section: issue
---

The last major component of the postmarketOS infrastructure is being moved to a new host. This
runs mirror.postmarketos.org, build.postmarketos.org and pkgs.postmarketos.org. Some downtime
is expected during this migration.
