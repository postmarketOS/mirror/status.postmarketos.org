---
title: Domain registrar move
date: 2020-06-08 19:35:00
resolved: true
resolvedWhen: 2020-06-08 20:35:00
# down, disrupted, notice
severity: disrupted
affected:
  - Wiki
  - Build manager
  - Website
  - Binary repository
  - Matrix
section: issue
---

*Resolved* - nameserver move is done, everything seems up again.

The domain name postmarketos.org is being moved between registrars and this is causing some issues with nameservers. The issues should resolve itself in an hour.
