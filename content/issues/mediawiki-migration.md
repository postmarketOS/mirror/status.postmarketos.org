---
title: Mediawiki migration and upgrade
date: 2020-08-07 14:43:00
resolved: true
resolvedWhen: 2020-08-07 16:31:00
# down, disrupted, notice
severity: disrupted
affected:
  - Wiki
section: issue
---

*Monitoring* -
Looks like the wiki is fully working, even the mobile view. {{< track "2020-08-07 16:31:00" >}}

*Disrupted* - 
Base wiki is up again, now resolving issues with outdated extensions. {{< track "2020-08-07 16:09:00" >}}

The wiki is being migrated to another server and has to be upgraded to the
latest release to be able to run on PHP 7.3, due to it being mediawiki this
upgrade will take a long while.
