---
title: Migration to gitlab.postmarketos.org
date: 2024-10-05 22:00:00
resolved: false
# down, disrupted, notice
severity: disrupted
affected:
  - GitLab
section: issue
---

On Sunday, 2024-10-06 00:00 CEST, we are migrating from gitlab.com to our
self-hosted instance at gitlab.postmarketos.org. It is expected to take the
whole day (a test import took 18 hours).

* Projects (pmbootstrap, pmaports, etc.) will be read-only during the
  migration, meaning that you can't create issues, merge requests or comment on
  these.

* Accounts in gitlab.postmarketos.org will be approved *after* the migration is
  done.

* Read [postmarketos#77](https://gitlab.com/postmarketOS/postmarketos/-/issues/77)
  for more information on the migration plan, why we migrate to a self-hosted
  gitlab instance in the first place and why to that instead of Forgejo,
  SourceHut, etc.
